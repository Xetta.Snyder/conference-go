import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def minecraft_cordinates(city, state):
    state = state.abbreviation
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}")
    john = json.loads(response.text)[0]
    awsome_sauce = john["lat"]
    awsome_sauce2 = john["lon"]
    return awsome_sauce, awsome_sauce2


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state,}
    url = f"https://api.pexels.com/v1/search"
    response = requests.get(
        url,
        params=params,
        headers=headers,
    )
    jhon = json.loads(response.text)
    p_url = jhon["photos"][0]["src"]["original"]
    return p_url


# clean this up later
def get_weather_data(city, state):
    lat, lon = minecraft_cordinates(city=city, state=state)
    wethurl = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(wethurl)
    john = json.loads(response.text)
    descript = john["weather"][0]["description"]
    temp = john["main"]["temp"]

    return {"temp": temp, "description": descript}
